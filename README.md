# react-native-mi-snap-sdk

## Getting started

`$ npm install react-native-mi-snap-sdk --save`


### Installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-mi-snap-sdk` and add `RNMiSnapSdk.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNMiSnapSdk.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNMiSnapSdkPackage;` to the imports at the top of the file
  - Add `new RNMiSnapSdkPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':api-release', ':misnapcamera-release', ':mibidata-release', ':imageutils-release', ':misnapscience-release', ':sanselan-release', ':misnapcontroller', ':barcodecontroller', ':barcode-release', ':creditcardcontroller', ':cardio-release', ':misnapworkflow', ':misnapworkflow_UX2'
    project(':api-release').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-mi-snap-sdk/android/src/main/libs/api-release')
    project(':misnapcamera-release').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-mi-snap-sdk/android/src/main/libs/misnapcamera-release')
    project(':mibidata-release').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-mi-snap-sdk/android/src/main/libs/mibidata-release')
    project(':imageutils-release').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-mi-snap-sdk/android/src/main/libs/imageutils-release')
    project(':misnapscience-release').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-mi-snap-sdk/android/src/main/libs/misnapscience-release')
    project(':sanselan-release').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-mi-snap-sdk/android/src/main/libs/sanselan-release')
    project(':misnapcontroller').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-mi-snap-sdk/android/src/main/libs/misnapcontroller')
    project(':barcodecontroller').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-mi-snap-sdk/android/src/main/libs/barcodecontroller')
    project(':barcode-release').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-mi-snap-sdk/android/src/main/libs/barcode-release')
    project(':creditcardcontroller').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-mi-snap-sdk/android/src/main/libs/creditcardcontroller')
    project(':cardio-release').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-mi-snap-sdk/android/src/main/libs/cardio-release')
    project(':misnapworkflow').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-mi-snap-sdk/android/src/main/libs/misnapworkflow')
    project(':misnapworkflow_UX2').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-mi-snap-sdk/android/src/main/libs/misnapworkflow_UX2')


    include ':react-native-mi-snap-sdk'
    project(':react-native-mi-snap-sdk').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-mi-snap-sdk/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-mi-snap-sdk')
  	```
4. Add project.ext to your projects build.gradle and setup your project variables :
  	```
    project.ext {
        targetSdkVersion = 26
        compileSdkVersion = 27
        coreMinSdk = 16
    
        supportVersion = '27.1.1'
        eventbusVersion = '3.1.1'
        constraintLayoutVersion = '1.1.0-beta4'
    
        dexmakerVersion = '2.16.0'
        supportTestVersion = '1.0.1'
        mockitoVersion = '1.10.19'
        robotiumVersion = '5.6.3'
        espressoVersion = '3.0.1'
        leakcanaryVersion = '1.5.4'
        robolectricVersion = '3.7.1'
        junitVersion = '4.12'
        androidAssertJVersion = '1.2.0'
        guavaVersion = '23.1-android'
        cardioVersion = '5.3.0'
    }
  	```
## Usage
```javascript
import RNMiSnapSdk from 'react-native-mi-snap-sdk';

// TODO: What to do with the module?
RNMiSnapSdk;
```
  