
package com.reactlibrary;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.net.Uri;
import android.app.Activity;
import org.json.JSONException;
import org.json.JSONObject;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.Promise;

import com.miteksystems.misnap.analyzer.MiSnapAnalyzerResult;
import com.miteksystems.misnap.misnapworkflow.MiSnapWorkflowActivity;
import com.miteksystems.misnap.misnapworkflow_UX2.MiSnapWorkflowActivity_UX2;
import com.miteksystems.misnap.misnapworkflow_UX2.params.WorkflowApi;
import com.miteksystems.misnap.params.BarcodeApi;
import com.miteksystems.misnap.params.CameraApi;
import com.miteksystems.misnap.params.CreditCardApi;
import com.miteksystems.misnap.params.MiSnapApi;
import com.miteksystems.misnap.params.ScienceApi;

public class RNMiSnapSdkModule extends ReactContextBaseJavaModule implements ActivityEventListener {
    //  private static final String TAG = "MainActivity";
    private static int mGeoRegion = ScienceApi.GEO_REGION_GLOBAL;
    private static final long PREVENT_DOUBLE_CLICK_TIME_MS = 1000;
    private static final String ERR_CANCELLED = "CANCELLED";
    private static final String FAILED_TO_PICK = "FAILED_TO_PICK";
    private static final String NO_ACTIVITY = "NO_ACTIVITY";
    private long mTime;
    private final ReactApplicationContext reactContext;
    private static int mUxWorkflow = 2;
    private Promise resultPromise;

    public RNMiSnapSdkModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
        reactContext.addActivityEventListener(this);
    }
    @ReactMethod
    public Promise startFlow(String docType, final Promise promise) {
        Activity currentActivity = getCurrentActivity();

        if (currentActivity != null) {
            resultPromise = promise;
        } else {
            promise.reject(NO_ACTIVITY, "Activity doesn't exist");
            return promise;
        }
        this.startMiSnapWorkflow(docType);
        return resultPromise;
    }
    private void startMiSnapWorkflow(String docType) {
        // Prevent multiple MiSnap instances by preventing multiple button presses
        if (System.currentTimeMillis() - mTime < PREVENT_DOUBLE_CLICK_TIME_MS) {
//        Log.e("UxStateMachine", "Double-press detected");
            return;
        }

        mTime = System.currentTimeMillis();
        JSONObject miSnapParams = new JSONObject();
        try {
            miSnapParams.put(MiSnapApi.MiSnapDocumentType, docType);
            if (docType.equals(MiSnapApi.PARAMETER_DOCTYPE_CHECK_FRONT)) {
                miSnapParams.put(ScienceApi.MiSnapGeoRegion, mGeoRegion);
            }


            // Example of how to add additional barcode scanning options
            if (docType.equals(MiSnapApi.PARAMETER_DOCTYPE_BARCODES)) {
                // Set everything except Code 128 because it conflicts w/ the PDF417 barcode on the back of most drivers licenses.
                miSnapParams.put(BarcodeApi.BarCodeTypes, BarcodeApi.BARCODE_ALL - BarcodeApi.BARCODE_CODE_128);
            }

            // Here you can override optional API parameter defaults
            miSnapParams.put(CameraApi.MiSnapAllowScreenshots, 1);
            // e.g. miSnapParams.put(MiSnapApi.AppVersion, "1.0");
            // Workflow parameters are now put into the same JSONObject as MiSnap parameters
            miSnapParams.put(WorkflowApi.MiSnapTrackGlare, "1");
            miSnapParams.put(CameraApi.MiSnapFocusMode, CameraApi.PARAMETER_FOCUS_MODE_HYBRID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Intent intentMiSnap;
//      activity  = this.getCurrentActivity();
        if (mUxWorkflow == 1) {
            intentMiSnap = new Intent(this.getCurrentActivity(), MiSnapWorkflowActivity.class);
        } else {
            intentMiSnap = new Intent(this.getCurrentActivity(), MiSnapWorkflowActivity_UX2.class);
        }
        intentMiSnap.putExtra(MiSnapApi.JOB_SETTINGS, miSnapParams.toString());
        reactContext.startActivityForResult(intentMiSnap, MiSnapApi.RESULT_PICTURE_CODE,null);
    }
    @Override
    public void onActivityResult(Activity activity, final int requestCode, final int resultCode, final Intent intent) {
        Bundle extras = intent.getExtras();
        if (requestCode == MiSnapApi.RESULT_PICTURE_CODE){
            if (resultCode == Activity.RESULT_CANCELED) {
                resultPromise.reject(ERR_CANCELLED, "Canceled by user");
            } else if (resultCode == Activity.RESULT_OK) {
                try{
                    byte[] sImage = intent.getByteArrayExtra(MiSnapApi.RESULT_PICTURE_DATA);
                    byte[] sEncodedImage = Base64.encode(sImage, Base64.DEFAULT);
                    resultPromise.resolve(new String(sEncodedImage));
                } catch (Exception e){
                    resultPromise.reject(e.getMessage(), e.getMessage());
                }
            }
        }
    }
    @Override
    public void onNewIntent(Intent intent) {
        //super.onNewIntent(intent);
    }
    @Override
    public String getName() {
        return "RNMiSnapSdk";
    }
}