using ReactNative.Bridge;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace Mi.Snap.Sdk.RNMiSnapSdk
{
    /// <summary>
    /// A module that allows JS to share data.
    /// </summary>
    class RNMiSnapSdkModule : NativeModuleBase
    {
        /// <summary>
        /// Instantiates the <see cref="RNMiSnapSdkModule"/>.
        /// </summary>
        internal RNMiSnapSdkModule()
        {

        }

        /// <summary>
        /// The name of the native module.
        /// </summary>
        public override string Name
        {
            get
            {
                return "RNMiSnapSdk";
            }
        }
    }
}
